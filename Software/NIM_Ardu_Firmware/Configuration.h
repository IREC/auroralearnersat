/***************************************************
  This is the firmware for the Northern Images Mission (NIM) DemoSat.

  This sketch uses libraries provided by Adafruit and ArduSat. Please see their distribution requirements below.

  Configuration.h

  Please use the following preprocessor commands to fine tune the behaviour of your NIM demosat.
*/

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

/*
  * SCREEN CONFIGURATION
*/

#define SSD1351

enum menuNavigationEvents {
  btnEncodeOK, //5
  encoderUp, //6
  encoderDown, //7
  btnNone //8
};

#define BUFFPIXEL 20

//MENU TYPE
//Screen Parameters
#define LINE_COUNT 3
#define CHAR_COUNT 16
#define MENU_SIZE 4
#define CHAR_SPACING 20

#define SCREEN_SUPPORTS_PRINTLN
//#define SCREEN_SUPPORTS_SET_CURSOR

//SCREEN SIZE - Change to suit screen module
#define SCREEN_WIDTH  128
#define SCREEN_HEIGHT 128 // Change this to 96 for 1.27" OLED.


// You can use any (4 or) 5 pins
//#define SCLK_PIN 7
//#define MOSI_PIN 8
#define DC_PIN   4
#define CS_PIN   5
#define RST_PIN  6

// Color definitions
#define	BLACK           0x0000
#define	BLUE            0x001F
#define	RED             0xF800
#define	GREEN           0x07E0
#define CYAN            0x07FF
#define MAGENTA         0xF81F
#define YELLOW          0xFFE0
#define WHITE           0xFFFF
/*
  * CAMERA CONFIGURATION
*/



/*
  * SD MODULE CONFIGURATION
*/
#define SD_CHIP_SELECT 10

/*
  * ENCODER SETUP
*/

#define Encoder_A 3
#define Encoder_B 2
#define Encoder_SW 7

#endif
