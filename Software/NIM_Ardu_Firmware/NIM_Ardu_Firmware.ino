
/***************************************************
  This is the firmware for the Northern Images Mission (NIM) DemoSat.

  This sketch uses libraries provided by Adafruit and ArduSat. Please see their distribution requirements below.

  NIM_Ardu_Firmware.ino

  For more information, please get in touch cubesat@nwtresearch.comxx
*/

/***************************************************
  This is a example sketch demonstrating graphic drawing
  capabilities of the SSD1351 library for the 1.5"
  and 1.27" 16-bit Color OLEDs with SSD1351 driver chip

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/products/1431
  ------> http://www.adafruit.com/products/1673

  If you're using a 1.27" OLED, change SCREEN_HEIGHT to 96 instead of 128.

  These displays use SPI to communicate, 4 or 5 pins are required to
  interface
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
*/

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1351.h>
#include <Adafruit_VC0706.h>
#include <SD.h>
#include <SPI.h>
#include <SoftwareSerial.h>
#include "Configuration.h"
#include "MenuManager.h"
#include <TimerOne.h>

//Menu Manager - menu
uint8_t encoderFlag = btnNone;
MenuManager menu = MenuManager();
bool inMenu = true;

//Camera Module - cam
SoftwareSerial cameraconnection = SoftwareSerial(9, 8); //(RX,TX)
Adafruit_VC0706 cam = Adafruit_VC0706(&cameraconnection);

File     bmpFile;
int      bmpWidth, bmpHeight;   // W+H in pixels
uint8_t  bmpDepth;              // Bit depth (currently must be 24)


void setup() {
  PROGMEM String serialStartString = "Serial Started";
  PROGMEM String nimStartMessage = "NIM Started";
  PROGMEM String SDSuccessString = "SD Found";
  PROGMEM String SDFailString = "SD Failed";

  //Start Communucations
  Serial.begin(9600);
  Serial.println(serialStartString);
  consoleLog(nimStartMessage);
  pinMode(SD_CHIP_SELECT,OUTPUT);
  //Start SD
  while(!SD.begin(SD_CHIP_SELECT)) {
    consoleLog(SDFailString);
    delay(2000);
  }
  consoleLog(SDSuccessString);
  //Start Timer1
  //Timer1.initialize(5000000);
  //Timer1.attachInterrupt(reportMemory);
  //Start Camera
  // Try to locate the camera
  while(!cam.begin()) {
    consoleLog("NoCam");
    delay(1000);
    return;
  }
  consoleLog("CamFound:");
  // Print out the camera version information (optional)
  char *reply = cam.getVersion();
  if (reply == 0) {
    consoleLog("FailID");
  } else {
    consoleLog(reply);
  }
  cam.setImageSize(VC0706_640x480);        // biggest
  //cam.setImageSize(VC0706_320x240);        // medium
  //cam.setImageSize(VC0706_160x120);          // small
  //Add Menu Items
  menu.begin();
  PROGMEM String selectImageString = "Select Pic";
  menu.addMenuItem(0,selectImageString,selectImage);
  PROGMEM String captureImageString = "Capture";
  menu.addMenuItem(1,captureImageString,captureImage);
  PROGMEM String resetCameraString = "Reset Camera";
  menu.addMenuItem(2,resetCameraString,resetCamera);
  PROGMEM String testScreenString = "Test Screen";
  menu.addMenuItem(3,testScreenString, testScreen);
  //Start Encoder
  pinMode(Encoder_A, INPUT_PULLUP);
  pinMode(Encoder_B, INPUT_PULLUP);
  pinMode(Encoder_SW, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(Encoder_B), encoder_Update, FALLING); // 1 is interupt on Encoder_A
  clearScreen();
  inMenu = true;
  testScreen();
  menu.updateScreen();
}
void loop() {
  //Check Mode Booleans and act.
  while(inMenu) {
    if(!digitalRead(Encoder_SW))
    {
      encoderFlag = btnEncodeOK;
    }
    noInterrupts();
    if(encoderFlag != btnNone) {
      consoleLog("Event Found");
      menu.handleNavigationEvent(encoderFlag);
      encoderFlag = btnNone;
      consoleLog("Event Handled");
    }
    interrupts();
  }
}
void testScreen() {
  consoleLog("Testing Screen");
  menu.testScreen();
  consoleLog("Test Complete");
}
void clearScreen() {
  menu.tft.fillScreen(BLACK);
}
void consoleLog(String message) {
  noInterrupts();
  Serial.print(millis());
  Serial.print("- ");
  Serial.println(message);
  interrupts();
}
void captureImage() {
  resetCamera();
  bmpDraw("test.bmp",0,0);
  //Temp
  //menu.tft.fillScreen(BLACK);
  //menu.tft.setTextSize(2);
  //menu.tft.setCursor(1,1);
  //menu.tft.println("Hello!");
  //menu.tft.println("from IREC");
  delay(1000);
  if (!cam.takePicture()) {
    consoleLog("Failed to Snap!");
    menu.tft.println("Failed to Snap!");
    delay(1000);
  }
  else {
    consoleLog("Picture taken!");
    // Create an image with the name IMAGExx.JPG
    char filename[13];
    strcpy(filename, "IMAGE00.JPG");
    for (int i = 0; i < 100; i++) {
      filename[5] = '0' + i/10;
      filename[6] = '0' + i%10;
      // create if does not exist, do not open existing, write, sync after write
      if (! SD.exists(filename)) {
        break;
      }
    }
    // Open the file for writing
    File imgFile = SD.open(filename, FILE_WRITE);
    // Get the size of the image (frame) taken
    uint16_t jpglen = cam.frameLength();
    menu.tft.fillScreen(BLACK);
    menu.tft.setTextSize(1);
    menu.tft.setCursor(10,10);
    menu.tft.println("Storing Image");
    consoleLog("Storing ");
    Serial.println(jpglen);
    consoleLog(" byte image.");
    int32_t time = millis();
    // Read all the data up to # bytes!
    byte wCount = 0; // For counting # of writes
    while (jpglen > 0) {
      // read 32 bytes at a time;
      uint8_t *buffer;
      uint8_t bytesToRead = min(64, jpglen); // change 32 to 64 for a speedup but may not work with all setups!
      buffer = cam.readPicture(bytesToRead);
      imgFile.write(buffer, bytesToRead);
      if(++wCount >= 64) { // Every 2K, give a little feedback so it doesn't appear locked up
        consoleLog("Transfering Image");
        wCount = 0;
      }
      //Serial.print("Read ");  Serial.print(bytesToRead, DEC); Serial.println(" bytes");
      jpglen -= bytesToRead;
    }
    imgFile.close();
    resetCamera();
    consoleLog("Capture Completed");
  }
}
void getCameraVersion() {
  char *reply = cam.getVersion();
  menu.tft.fillScreen(BLACK);
  menu.tft.setCursor(0,0);
  if (reply == 0) {
    consoleLog("Failed to ID");
    menu.tft.println("Failed to ID");
  } else {
    consoleLog(reply);
    menu.tft.println(reply);
  }
  delay(1000);
}
void resetCamera() {
    menu.tft.fillScreen(BLACK);
    menu.tft.setCursor(0,0);
    menu.tft.println("Reset Camera");
    cam.reset();
    delay(100);
    getCameraVersion();
}
void selectImage() {
  menu.tft.fillScreen(BLACK);
  menu.tft.println("ComingSoon");
  delay(500);
  menu.tft.fillScreen(BLACK);
} //To Be Implemented
void bmpDraw(char *filename, uint8_t x, uint8_t y) {
  uint32_t bmpImageoffset;        // Start of image data in file
  uint32_t rowSize;               // Not always = bmpWidth; may have padding
  uint8_t  sdbuffer[3*BUFFPIXEL]; // pixel buffer (R+G+B per pixel)
  uint8_t  buffidx = sizeof(sdbuffer); // Current position in sdbuffer
  boolean  goodBmp = false;       // Set to true on valid header parse
  boolean  flip    = true;        // BMP is stored bottom-to-top
  int      w, h, row, col;
  uint8_t  r, g, b;
  uint32_t pos = 0;
  uint32_t startTime = millis();
  if((x >= menu.tft.width()) || (y >= menu.tft.height())) return;
  consoleLog("LoadingImage");
  consoleLog(String(filename));
  // Open requested file on SD card
  if ((bmpFile = SD.open(filename)) == NULL) {
    consoleLog("FileNotFound");
    return;
  }
  // Parse BMP header
  if(read16(bmpFile) == 0x4D42) { // BMP signature
    consoleLog("FileSize:"); Serial.println(read32(bmpFile));
    (void)read32(bmpFile); // Read & ignore creator bytes
    bmpImageoffset = read32(bmpFile); // Start of image data
    consoleLog("ImageOffset:"); Serial.println(bmpImageoffset, DEC);
    // Read DIB header
    consoleLog("HeaderSize:"); Serial.println(read32(bmpFile));
    bmpWidth  = read32(bmpFile);
    bmpHeight = read32(bmpFile);
    if(read16(bmpFile) == 1) { // # planes -- must be '1'
      bmpDepth = read16(bmpFile); // bits per pixel
      consoleLog("BitDepth:"); Serial.println(bmpDepth);
      if((bmpDepth == 24) && (read32(bmpFile) == 0)) { // 0 = uncompressed

        goodBmp = true; // Supported BMP format -- proceed!
        consoleLog("ImageSize:");
        Serial.println(bmpWidth);
        consoleLog("x");
        Serial.println(bmpHeight);
        // BMP rows are padded (if needed) to 4-byte boundary
        rowSize = (bmpWidth * 3 + 3) & ~3;
        // If bmpHeight is negative, image is in top-down order.
        // This is not canon but has been observed in the wild.
        if(bmpHeight < 0) {
          bmpHeight = -bmpHeight;
          flip      = false;
        }
        // Crop area to be loaded
        w = bmpWidth;
        h = bmpHeight;
        if((x+w-1) >= menu.tft.width())  w = menu.tft.width()  - x;
        if((y+h-1) >= menu.tft.height()) h = menu.tft.height() - y;

        for (row=0; row<h; row++) { // For each scanline...
          // tft.goTo(x, y+row);
          // Seek to start of scan line.  It might seem labor-
          // intensive to be doing this on every line, but this
          // method covers a lot of gritty details like cropping
          // and scanline padding.  Also, the seek only takes
          // place if the file position actually needs to change
          // (avoids a lot of cluster math in SD library).
          if(flip) // Bitmap is stored bottom-to-top order (normal BMP)
            pos = bmpImageoffset + (bmpHeight - 1 - row) * rowSize;
          else     // Bitmap is stored top-to-bottom
            pos = bmpImageoffset + row * rowSize;
          if(bmpFile.position() != pos) { // Need seek?
            bmpFile.seek(pos);
            buffidx = sizeof(sdbuffer); // Force buffer reload
          }

          // optimize by setting pins now
          for (col=0; col<w; col++) { // For each pixel...
            // Time to read more pixel data?
            if (buffidx >= sizeof(sdbuffer)) { // Indeed
              bmpFile.read(sdbuffer, sizeof(sdbuffer));
              buffidx = 0; // Set index to beginning
            }

            // Convert pixel from BMP to TFT format, push to display
            b = sdbuffer[buffidx++];
            g = sdbuffer[buffidx++];
            r = sdbuffer[buffidx++];

            menu.tft.drawPixel(x+col, y+row, menu.tft.color565(r,g,b));
            // optimized!
            //tft.pushColor(tft.color565(r,g,b));
          } // end pixel
        } // end scanline
        consoleLog("LoadedIn");
        consoleLog(millis() - startTime);
        consoleLog("ms");
      } // end goodBmp
    }
  }
  bmpFile.close();
  if(!goodBmp) consoleLog("BMPNotRecognized");
}
uint16_t read16(File f) {
  uint16_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read(); // MSB
  return result;
}
uint32_t read32(File f) {
  uint32_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read();
  ((uint8_t *)&result)[2] = f.read();
  ((uint8_t *)&result)[3] = f.read(); // MSB
  return result;
}
void encoder_Update() {
  if(encoderFlag == btnNone) {
    if(!digitalRead(Encoder_A)) {
  		encoderFlag = encoderUp;
      consoleLog("EncUP");
    }
    else {
  		encoderFlag = encoderDown;
      consoleLog("EncDOWN");
    }
  }
}
