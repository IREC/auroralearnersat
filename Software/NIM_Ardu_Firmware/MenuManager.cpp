#include "MenuManager.h"

MenuManager::MenuManager() {
  //Construct the blank menu here!
  for(int i = 0;i<MENU_SIZE;i++)
  {
    functionDescriptor[i] = "0" + (String)i;
  }
  //lcd.begin(CHAR_COUNT,LINE_COUNT); // From Previous Screen Model
}
void MenuManager::begin()
{
  tft.begin();
  tft.fillScreen(BLACK);
  tft.setTextSize(1);
  tft.setRotation(2);
}
void MenuManager::updateScreen() {
  //Draw the current screen
  tft.fillScreen(BLACK);
  //Draw lines
  for(int lineIndex = 0; lineIndex < LINE_COUNT; lineIndex++)  {
    tft.setCursor(CHAR_SPACING,CHAR_SPACING*lineIndex);
    tft.print(functionDescriptor[menuIndex+lineIndex]);
    //Serial.println("Menu Update: " + functionDescriptor[menuIndex+lineIndex]);
  }
  //Draw Cursor
  tft.setCursor(0,cursorPosition*CHAR_SPACING);
  tft.print(">");
}
void MenuManager::handleNavigationEvent(int eventFlag) {
  //Update the indeces based on an eventFlag
  if(eventFlag == encoderDown) {
    if(cursorPosition < LINE_COUNT-1) {
      cursorPosition++;
    }
    else {
      if(menuIndex == MENU_SIZE-LINE_COUNT) {
        menuIndex = 0;
        cursorPosition = 0;
      }
      else {
        menuIndex++;
        cursorPosition--;
      }

    }
  }
  else if(eventFlag == encoderUp) {
    if(cursorPosition > 0) {
      cursorPosition--;
    }
    else {
      if(menuIndex == 0) {
        menuIndex = MENU_SIZE-LINE_COUNT;
        cursorPosition = LINE_COUNT-1;
      }
      else {
        menuIndex--;
        cursorPosition++;
      }
    }
  }
  else if(eventFlag == btnEncodeOK) {
    menuFunc[cursorPosition + menuIndex](0);
  }
  updateScreen();
}
void MenuManager::addMenuItem(uint8_t index,String text,void newMenuFunction(int)) {
  functionDescriptor[index] = text;
  menuFunc[index] = newMenuFunction;
}

/*
  * Screen Access FUNCTIONS
*/



/*
  * Screen Test Functions
*/

void MenuManager::testScreen() {
  lcdTestPattern();
  delay(2000);
  #ifdef SSD1351
    tft.fillScreen(BLACK);
  #endif
}
/**************************************************************************/
/*!
    @brief  Renders a simple test pattern on the screen
*/
/**************************************************************************/
void MenuManager::lcdTestPattern(void)
{
  static const uint16_t PROGMEM colors[] =
    { RED, YELLOW, GREEN, CYAN, BLUE, MAGENTA, BLACK, WHITE };

  for(uint8_t c=0; c<8; c++) {
    tft.fillRect(0, tft.height() * c / 8, tft.width(), tft.height() / 8,
      pgm_read_word(&colors[c]));
  }
}
