#include <Arduino.h>
#include <LiquidCrystal.h>
#include <string.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1351.h>
#include "Configuration.h"
#include <SPI.h>


class MenuManager
{
  public:
  // * CONSTRUCTOR
  MenuManager();
  // * DATA
  //LiquidCrystal lcd = LiquidCrystal(8, 9, 4, 5, 6, 7); For a previous screen model
  Adafruit_SSD1351 tft = Adafruit_SSD1351(SCREEN_WIDTH, SCREEN_HEIGHT, &SPI, CS_PIN, DC_PIN, RST_PIN);
  void (*menuFunc[MENU_SIZE])(int);
  String functionDescriptor[MENU_SIZE];
  // Current state of the screen
  uint8_t menuIndex = 0;
  uint8_t cursorPosition = 0;
  // * Menu FUNCTIONS
  void begin();
  void addMenuItem(uint8_t,String,void menuFunction(int));
  void updateScreen();
  void handleNavigationEvent(int);
  // * Screen Access FUNCTIONS

  // * Screen Test Functions
  void testScreen();
  void lcdTestPattern(void);

};
